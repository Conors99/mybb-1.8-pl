<?php
/**
* MyBB 1.8 Polish Language Pack
 * Copyright © 2008-2016 MyBBoard.pl Team
 * See readme.html for copyright information. | Zapoznaj sie z plikiem czytaj_to.html.
 * Autorzy: Conors, Divir
 */

$l['nav_announcements'] = "Ogłoszenia";
$l['announcements'] = "głoszenia";
$l['forum_announcement'] = "Ogłoszenie: {1}";
$l['error_invalidannouncement'] = "Wybrane ogłoszenie jest nieprawidłowe.";

$l['announcement_edit'] = "Edytuj ogłoszenie";
$l['announcement_qdelete'] = "Usuń ogłoszenie";
$l['announcement_quickdelete_confirm'] = "Na pewno chcesz usunąć to ogłoszenie?";

